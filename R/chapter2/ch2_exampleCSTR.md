# Chapter2: Nonlinear Algebraic Systems

This is from the book:

Numerical Methods in Chemical Engineering: Applications in MATLAB by Kenneth Beers


## Example: CSTR at steady state

There are two reactions in the CSTR
$$ A + B \rightarrow C \;\; r_{R1}=k_1c_Ac_B \\
C + B \rightarrow D \;\; r_{R2}=k_2c_Ac_B
$$
  
Steady state concentration obtained by solving the following set of nonlinear equations
$$
v(c_{A,in}-c_A)+V(-k_1c_Ac_B)=0 \\
v(c_{B,in}-c_B)+V(-k_1c_Ac_B-k_2c_Bc_C)=0 \\
v(c_{c,in}-c_C)+V(k_1c_Ac_B-k_2c_Bc_C)=0 \\
v(c_{D,in}-c_D)+V(k_2c_Bc_C)=0
$$
  

```r
library(nleqslv)

# Set inlet flow rate and concentration
v=1
V=100
c_in=c(A=1,B=2,C=0,D=0)

# Set rate constants
k1=1
k2=1

# function specifying equations whose solution is needed
fn=function(c,parms){
  
  f=rep(0,length(c))
  
  k=parms$k
  v=parms$v
  V=parms$V
  c_in=parms$c_in
  
  r=rep(0,length(k))
  
  r[1]=k[1]*c["A"]*c["B"]
  r[2]=k[2]*c["B"]*c["C"]
  
  f[1]=v*(c_in["A"]-c["A"])+V*(-r[1])
  f[2]=v*(c_in["B"]-c["B"])+V*(-r[1]-r[2])
  f[3]=v*(c_in["C"]-c["C"])+V*(r[1]-r[2])
  f[4]=v*(c_in["D"]-c["D"])+V*r[2]
  
  return(f)
  
}

# set parameters
parms=list(v=v,V=V,c_in=c_in,k=c(k1,k2))

# initial guess for concentration
c0=c_in

# solve the equations
x=nleqslv(c0,fn,parms=parms)
# solution
x$x
```

```
##       A       B       C       D 
## 0.05661 0.16664 0.05341 0.88998
```


## Example: Solution using a simple version of homotopy method

The system is solved initially for high value of flow rate $v$ and in subsequent iterations the value of $v$ is gradually reduced to actual $v$. The initial guess for each iteration is the solution from the previous iteration.


```r

# flowrate for starting iteration
vinit=v*1000

# set the sequence of flowrates for iterations
vseq=10^seq(log10(vinit),log10(v),length.out=5)

c0=c_in # inital guess for first iteration

for(i in 1:length(vseq)){
  
  # set parameters
  parms=list(v=vseq[i],V=V,c_in=c_in,k=c(k1,k2))

  # solve the equations
  x=nleqslv(c0,fn,parms=parms)
  
  # set solution as initial guess for next iteration
  c0=x$x
  print(c(iter=i,c0))
  
}
```

```
##    iter       A       B       C       D 
## 1.00000 0.84587 1.82212 0.13037 0.02376 
##   iter      A      B      C      D 
## 2.0000 0.5641 1.3741 0.2459 0.1900 
##   iter      A      B      C      D 
## 3.0000 0.2881 0.7813 0.2051 0.5068 
##   iter      A      B      C      D 
## 4.0000 0.1305 0.3746 0.1135 0.7560 
##    iter       A       B       C       D 
## 5.00000 0.05661 0.16664 0.05341 0.88998
```

```r

# solution to the actual problem is the solution of the last iteration
print(x$x)
```

```
##       A       B       C       D 
## 0.05661 0.16664 0.05341 0.88998
```



```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] nleqslv_2.0 knitr_1.2  
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    stringr_0.6.2 
## [5] tools_3.0.1
```

