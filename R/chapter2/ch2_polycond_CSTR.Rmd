Chapter 2: Nonlinear Algebraic Systems
========================================================
This is from the book:

Numerical Methods in Chemical Engineering: Applications in MATLAB by Kenneth Beers

## Example: Steady-state modeling of condensation polymerization reactor

This system is governed by the solution of the following set of nonlinear equations

$$
f_1=\mu_0^{in}-\phi\mu_0-(Da)\mu_0^2+(Da)K_{eq}^{-1}\omega(1-\mu_0)=0 \\
f_2=\mu_2^{in}-\phi\mu_2+2(Da)+(Da)K_{eq}^{-1}\omega(\frac{1}{3}-\mu_3)=0 \\
f_3=\omega^{in}-\phi\omega-\gamma\omega+(Da)\mu_0^2-(Da)K_{eq}^{-1}\omega(1-\mu_0)=0 \\
\mu_3=\frac{\mu_2(2\mu_2\mu_0-1)}{\mu_0},\;\;\phi=1-\gamma\zeta\omega
$$

where we solve for $\mu_0$, $\mu_2$, and $\omega$. This is then used to compute number average chain length $x_n=\mu_1/\mu_0$, polydispersity $Z=\mu_2\mu_0$ and flowrate $\phi$. The inputs are:
$$ (Da,\gamma,\zeta,K_{eq},\omega^{in},\mu_0^{in},\mu_2^{in}) $$

```{r, tidy=FALSE}
# this code is based on the matlab code polycond_CSTR.m in book resources site

# load libraries
library(nleqslv)
library(deSolve)

# set input
parm=c(zeta=0.2, 
       Keq=100,
       win=0,
       mu0in=1,
       mu2in=1,
       Da=10^(-2),
       gamma=10^(-4))

# Define the function that specifies functions to be solved
fn=function(x,parm){

  mu0=x[1]
  mu2=x[2]
  w=x[3]
  
  phi=1-parm["gamma"]*parm["zeta"]*w
  mu3=mu2*(2*mu2*mu0-1)/mu0
  
  f=rep(0,3)
  f[1]=parm["mu0in"]-phi*mu0-parm["Da"]*mu0^2+parm["Da"]/parm["Keq"]*w*(1-mu0)
  f[2]=parm["mu2in"]-phi*mu2+2*parm["Da"]+parm["Da"]/parm["Keq"]*w*(1/3-mu3)
  f[3]=parm["win"]-phi*w-parm["gamma"]*w+parm["Da"]*mu0^2-parm["Da"]/parm["Keq"]*w*(1-mu0)                                                      
  
  return(f)                                                                      

}

# Define the function that specifies function to be solved as derivatives 
# so that they could be solved as ODEs to provide an initial guess for
# solving nonlinear equations
fnxdot=function(t,x,parms){
  f=fn(x,parms)
  return(list(f))
}
```

The system is solved over a grid of values of $Da$ and $\gamma$. 

```{r, tidy=FALSE}
# grid over log10(Da) and log10(gamma)
log10Da_grid=seq(-2,4,length.out=25)
log10gamma_grid=seq(-4,4,length.out=25)
 
nDa=length(log10Da_grid) # number of grid points for Da
ng=length(log10gamma_grid) # number of grid points for gamma

# initialize matrices of solution values
mu0_grid=matrix(0,nrow=ng,ncol=nDa)
mu2_grid=matrix(0,nrow=ng,ncol=nDa)
w_grid=matrix(0,nrow=ng,ncol=nDa)
phi_grid=matrix(0,nrow=ng,ncol=nDa)

# error flag for runs that did had errors and didn't provide a solution
erflag=matrix(0,nrow=ng,ncol=nDa)

# loop over the grid
for (i in 1:length(log10gamma_grid)){
  for (j in 1:length(log10Da_grid)){
    
    # for a given gamma, use inlet values as initial guess for the first Da
    # otherwise use solution at previous Da as the initial guess
    
    if(j == 1){
      xinit=c(parm["mu0in"],parm["mu2in"],parm["win"])
    } else {
      xinit=x$x
    }
    
    # set gamma and Da for the grid point
    parm["gamma"]=10^log10gamma_grid[i]
    parm["Da"]=10^log10Da_grid[j]
    
    # solve the ODE system to get a better initial guess (x at last time point)
    times=seq(0,10,0.1)
    test=lsoda(xinit,times=seq(0,10,0.1),func=fnxdot,parms=parm)
    xinit=test[nrow(test),2:ncol(test)]
    
    # solve the nonlinear system of equations
    x=nleqslv(xinit,fn,parm=parm)
    
    # store solution if nleqslv solved without errors (termcd=1)
    if(x$termcd == 1){
      mu0_grid[i,j]=x$x[1]
      mu2_grid[i,j]=x$x[2]
      w_grid[i,j]=x$x[3]
      phi_grid[i,j]=1-parm["gamma"]*parm["zeta"]*w_grid[i,j]
    } else {
      erflag[i,j]=1
    }
    
  }
}
```

Plot outputs as surface plots

```{r, tidy=FALSE}

# surface plot code taken from R help on persp

# Create a function interpolating colors in the range of specified colors
jet.colors <- colorRampPalette( c("blue", "green") )
# Generate the desired number of colors from this palette
nbcol <- 100
color <- jet.colors(nbcol)

# surface plot of w
z=w_grid
nrz=ng
ncz=nDa
zfacet <- z[-1, -1] + z[-1, -ncz] + z[-nrz, -1] + z[-nrz, -ncz]
facetcol=cut(zfacet,nbcol)
persp(log10gamma_grid,log10Da_grid,z,theta=-45,
       xlab="log10(gamma)",ylab="log10(Da)",
      col=color[facetcol],shade=0.75,ticktype="detailed")

# surface plot of phi
z=phi_grid
nrz=ng
ncz=nDa
zfacet <- z[-1, -1] + z[-1, -ncz] + z[-nrz, -1] + z[-nrz, -ncz]
facetcol=cut(zfacet,nbcol)
persp(log10gamma_grid,log10Da_grid,z,theta=-45,
       xlab="log10(gamma)",ylab="log10(Da)",
      col=color[facetcol],shade=0.75,ticktype="detailed")

# surface plot of Z
Z_grid=mu0_grid*mu2_grid
z=Z_grid
zfacet <- z[-1, -1] + z[-1, -ncz] + z[-nrz, -1] + z[-nrz, -ncz]
facetcol=cut(zfacet,nbcol)
persp(log10gamma_grid,log10Da_grid,z,theta=-45,
       xlab="log10(gamma)",ylab="log10(Da)",
      col=color[facetcol],shade=0.75,ticktype="detailed")

# surface plot of Xn
Xn_grid=1/mu0_grid
z=Xn_grid
zfacet <- z[-1, -1] + z[-1, -ncz] + z[-nrz, -1] + z[-nrz, -ncz]
facetcol=cut(zfacet,nbcol)
persp(log10gamma_grid,log10Da_grid,z,theta=-45,
        xlab="log10(gamma)",ylab="log10(Da)",
        col=color[facetcol],shade=0.75,ticktype="detailed")

```

## Session Information
This document was done in Rstudio and was generated with knitr

```{r, tidy=FALSE}
sessionInfo()
```

