Chapter 2: Nonlinear Algebraic Systems
========================================================
This is from the book:

Numerical Methods in Chemical Engineering: Applications in MATLAB by Kenneth Beers

## Search bifurcation points

The system of nonlinear equations to be solved for $x$ for a given value of parameter $\theta$ is:
$$ f(x;\theta)=0 $$

A search for bifurcation points along a linear path $\theta(\lambda)=(1-\lambda)\theta_0+\lambda\theta_1$ is done by solving the following augmented system:
$$
f(x;\theta(\lambda))=0 \\
|J(x;\theta(\lambda))|=0 
$$


```r
# library for solving nonlinear equations
library(nleqslv)

# define function
fn=function(x,parm){
  f=x^2+parm*x+1
  return(f)
}
 
# Estimate jacobian by finite difference
jac=function(x,parm){
  
  N=length(x)
  jac=matrix(0,nrow=N,ncol=N)
  delta=sqrt(.Machine$double.eps)
  f=fn(x,parm)
  
  for (i in 1:N){
    xnew=x
    xnew[i]=xnew[i]+delta
    fnew=fn(xnew,parm)
    jac[,i]=(fnew-f)/delta
    
  }
  
  return(jac)
  
}

# define augmented function for solving
augfn=function(augx){
  N=length(augx)
  x=augx[1:(N-1)]
  parm=parm0*(1-augx[N])+parm1*augx[N]
  
  f=matrix(0,nrow=N,ncol=1)
  f[1:(N-1)]=fn(x,parm)
  f[N]=det(jac(x,parm))
  
  return(f)
}

# set parm0 and parm1
parm0=0
parm1=4

# search along line from parm0 to parm1
# set sequence of search points
lamdaseq=seq(0,1,length.out=10)

# loop over search points to find any solution to augmented system of equations

xaugsol=matrix(NA,nrow=length(lamdaseq),ncol=2)

for(i in 1:length(lamdaseq)){

  # set initial guess for x and lamda
  xinit=0
  lamda=lamdaseq[i]
  augxinit=c(xinit,lamda)
  
  # solve augmented system
  test=nleqslv(augxinit,augfn)
  
  # if a solution is found record it
  if (test$termcd == 1){
    xaugsol[i,]=test$x
  }
  
}

# print found solutions
colnames(xaugsol)=c("x","theta")
xaugsol[!is.na(xaugsol[,1]),]
```

```
##       x theta
## [1,] -1   0.5
## [2,] -1   0.5
## [3,] -1   0.5
## [4,] -1   0.5
## [5,] -1   0.5
## [6,] -1   0.5
## [7,] -1   0.5
```


### Session Info

```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] nleqslv_2.0 knitr_1.2  
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    stringr_0.6.2 
## [5] tools_3.0.1
```

