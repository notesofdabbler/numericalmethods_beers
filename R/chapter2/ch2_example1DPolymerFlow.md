Chapter 2: Nonlinear Algebraic Systems
========================================================
This is from the book:

Numerical Methods in Chemical Engineering: Applications in MATLAB by Kenneth Beers

## Example: 1D-Polymer Flow Between Parallel Plates
 
Viscosity as a function of shear rate:
$$
\frac{\eta(\dot{\gamma})-\eta_{\infty}}{\eta_0-\eta_{\infty}}=
   [1+(\lambda\dot{\gamma})^a]^{(n-1)/a}
$$


```r
# set fluid parameters
fluid=list(eta0=1.48e4, # Pa.s
           etaInf=0,
           lambda=1.04, # s
           a=2,
           n=0.398
           )

# function relating shear rate to viscosity
calcVisc=function(gamma,fluid){
  
  normVisc=(1+(fluid$lambda*gamma)^fluid$a)^((fluid$n-1)/fluid$a)
  visc=fluid$etaInf+(fluid$eta0-fluid$etaInf)*normVisc
  return(visc)
  
}


# Make plot of shear dependent viscosity
gamma=10^(seq(-1,2,length.out=250))
visc=calcVisc(gamma,fluid)
df=data.frame(gamma=gamma,visc=visc)

library(ggplot2) # package for plots

p=ggplot(df,aes(x=gamma,y=visc))+geom_line()
p=p+scale_x_log10(limits=c(0.1,1e2),breaks=c(0.1,1,10,100))
p=p+scale_y_log10(limits=c(1e2,1e5),breaks=c(1e2,1e3,1e4,1e5))
p=p+xlab("shear rate in (1/s)")+ylab("viscosity in Pa s")
print(p)
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1.png) 


The system to be solved is:
$$
\eta(\dot{\gamma})\frac{dv_x}{dy}=
   \left(\frac{\Delta P}{\Delta x}\right)
   \left(y-\frac{B}{2}\right)
$$   

Using finite differences over the grid $y_k=k\Delta y,\;\Delta y=\frac{B}{N+1}$ results in the following system of equations
$$
f_k=\eta(\dot{\gamma}_k)\left[\frac{v_k-v_{k-1}}{\Delta y}\right]
   -\left(\frac{\Delta P}{\Delta x}\right)
    \left(y_k-\frac{B}{2}\right), \; k=1,2,\ldots,N
$$
where $v_0=0$ from the boundary condition and 
$$
\dot{\gamma}_k=\left|\frac{v_k-v_{k-1}}{\Delta y}\right|
$$


```r
# input specifications
Sys=list(B=10e-2) # distance between plate in m
      
# pressure gradient necessary to get a centerline velocity of 0.1m/s
# neglecting shear-thinning effects

#target velocity
vtar=0.1

Sys$dpdx=-8*fluid$eta0*vtar/Sys$B^2 # pressure gradient in Pa/m
Sys$dpdx
```

```
## [1] -1184000
```

```r

# Calculate approx Reynolds number to check if flow is in laminar conditions
Re=1000*vtar*Sys$B/fluid$eta0
Re
```

```
## [1] 0.0006757
```

```r

# input grid parameters
grid=list(3)
grid$N=100
grid$dy=Sys$B/(grid$N+1)
grid$y=seq(0,Sys$B,by=grid$dy)

# define functions to be solved
fn=function(v,Sys,fluid,grid){
  
  N=grid$N
  dy=grid$dy
  y=grid$y[2:(N+1)]
  B=Sys$B
  dpdx=Sys$dpdx
  
  f=rep(0,N)

  v_lag=c(0,v[1:(N-1)])
  gamma=abs((v-v_lag)/dy)
  visc=calcVisc(gamma,fluid)
  
  f=visc*((v-v_lag)/dy)-(dpdx)*(y-(B/2))
  
  return(f)
  
}

N=grid$N
y=grid$y[2:(N+1)]
v0=y*(y-Sys$B)/(2*fluid$eta0)*Sys$dpdx
```


Solve the system of equations. In MATLAB's fsolve function there is an option to specify the sparsity pattern of the jacobian. It doesn't appear to be there in nleqslv package solver. There are some other nonlinear equation solvers in R which have not been explored.


```r
library(nleqslv)

x=nleqslv(v0,fn,Sys=Sys,fluid=fluid,grid=grid)


# solution
vsol=x$x


# grid
y=grid$y[2:(N+1)]
# shear stress
shearStress=Sys$dpdx*(y-Sys$B/2)
# share rate
gamma=abs((vsol-c(0,vsol[1:(N-1)]))/grid$dy)
# viscosity
visc=calcVisc(gamma,fluid)

# Make plots
df=data.frame(y=grid$y[2:(N+1)],v0=v0,vsol=vsol,shearStress=shearStress,
              gamma=gamma,visc=visc)

source("multiplot.R") # enables plotting multiple graphs together

library(scales) # to change formatting of axis labels

p1=ggplot(data=df,aes(x=y,y=v0))+geom_line(col="red")+
  geom_line(aes(x=y,y=vsol),col="blue")+coord_flip()+
  xlab("y (m)")+ylab(expression(paste(v[x](y),"(m/s)")))+theme_bw(15)
p2=ggplot(data=df,aes(x=y,y=shearStress))+geom_line()+coord_flip()+
    scale_y_continuous(labels=scientific)+
    xlab("y (m)")+ylab(expression(paste(tau[yx],"(Pa)")))+theme_bw(15)
p3=ggplot(data=df,aes(x=y,y=gamma))+geom_line()+coord_flip()+
  xlab("y (m)")+ylab("shear-rate (1/s)")+theme_bw(15)
p4=ggplot(data=df,aes(x=y,y=visc))+geom_line()+coord_flip()+
    scale_y_continuous(labels=scientific)+
    xlab("y (m)")+ylab("viscosity (Pa s)")+theme_bw(15)

multiplot(p1,p2,p3,p4,cols=2)
```

```
## Loading required package: grid
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png) 



```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] grid      stats     graphics  grDevices utils     datasets  methods  
## [8] base     
## 
## other attached packages:
## [1] scales_0.2.3    nleqslv_2.0     ggplot2_0.9.3.1 knitr_1.2      
## 
## loaded via a namespace (and not attached):
##  [1] colorspace_1.2-2   dichromat_2.0-0    digest_0.6.3      
##  [4] evaluate_0.4.3     formatR_0.8        gtable_0.1.2      
##  [7] labeling_0.2       MASS_7.3-26        munsell_0.4       
## [10] plyr_1.8           proto_0.3-10       RColorBrewer_1.0-5
## [13] reshape2_1.2.2     stringr_0.6.2      tools_3.0.1
```

