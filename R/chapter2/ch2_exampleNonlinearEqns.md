Chapter 2: Nonlinear Algebraic Systems
========================================================

This is from the book:

Numerical Methods in Chemical Engineering: Applications in MATLAB by Kenneth Beers


## Example: Simple system of nonlinear equations

Solve the following set of nonlinear equations:
$$
f_1(x_1,x_2)=3x_1^3+4x_2^2-145=0 \\
f_2(x_1,x_2)=4x_1^2-x_2^3+28=0
$$

The jacobian for this system is:
$$
J=\left[
\begin{array}{cc}
9x_1^2 & 8x_2 \\
8x_1 & -3x_2^2
\end{array}
\right]
$$


```r
library(nleqslv)

# functions for which solution is needed
fn=function(x){
  
  f=rep(0,2)
  f[1]=3*x[1]^3+4*x[2]^2-145
  f[2]=4*x[1]^2-x[2]^3+28
  
  return(f)
  
}

# jacobian
jac=function(x){
  
  d=matrix(0,nrow=2,ncol=2)
  d[1,1]=9*x[1]^2
  d[1,2]=8*x[2]
  d[2,1]=8*x[1]
  d[2,2]=-3*x[2]^2
  
  return(d)
}

# inital guess for x
x0=c(1,1)

# solve the system of equations
x=nleqslv(x0,fn,jac)
x$x # the solution
```

```
## [1] 3 4
```


## Implementation of Newton's Method

### Graphical view of norm surface

Create a contour plot of the norm of function given by:
$$
\|f\|^2=f_1^2+f_2^2
$$
over the region
$$
-6 \leq x_1 \leq 6, \;\; -6 \leq x_2 \leq 6
$$


```r
x1=seq(-6,6,length.out=100)
x2=seq(-6,6,length.out=100)
X1X2=expand.grid(x1,x2)

fnorm=function(x) {
  fnorm=sqrt(sum(fn(x)*fn(x)))
  return(fnorm)
}

z=apply(X1X2,1,fnorm)
Z=matrix(z,nrow=100,ncol=100)

filled.contour(x1,x2,Z,nlevels=25,xlab="x1",ylab="x2",
      plot.axes={axis(1); axis(2);
                 contour(x1,x2,Z,nlevels=25,add=T)})
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2.png) 


### Newton's method

* Set a initial guess $x_0$
* The updated value for iteration $k+1$ given estimate $x_k$ at iteration $k$ is:
$$
x^{[k+1]}=x^{[k]}-J(x^{[k]})^{-1}f(x^{[k]})
$$


```r
# set initial guess
xinit=c(-4,-4)
# set max iterations
maxiter=100

# initialize matrix to hold values from each iteration
xiter=matrix(xinit,nrow=1,ncol=2)

# Iteratively update x until fnorm is less than a specified tolerance
for (iter in (2:maxiter)){
    x=xiter[iter-1,]
    dx=-solve(jac(x),fn(x))
    xnew=x+dx
    xiter=rbind(xiter,xnew)
    if (fnorm(xnew) <= 1e-10) { break }
}

# overlay newton's method trajectory on top of fnorm contour plot
filled.contour(x1,x2,Z,nlevels=25,xlab="x1",ylab="x2",
      plot.axes={axis(1); axis(2);
                 contour(x1,x2,Z,nlevels=25,add=T);
                 points(xiter[,1],xiter[,2],col="blue",pch=19);
                 lines(xiter[,1],xiter[,2],col="blue",lwd=2)})
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png) 


### Reduced-step Newton's method

Instead of taking full Newton step at each iteration, an additional line search is done to pick a point that reduces $\|f\|$ at each iteration.


```r
# set max iterations for convergence
maxiter=100

# max iterations for weak line search
maxiterLS=100

# set initial guess
x=c(1,1)

xiterrs=matrix(x,nrow=1,ncol=2)

for (iter in 2:maxiter){

  x=xiterrs[iter-1,]
  
  # calculate full Newton step
  dx=-solve(jac(x),fn(x))
  
  # sequence of division by 2
  fact=1/2^seq(0,maxiterLS)
  xLS=matrix(rep(x,length(fact)),nrow=length(fact),byrow=T)+
    fact*matrix(rep(dx,length(fact)),nrow=length(fact),byrow=T)
  
  # calculate fnorm at each point in line search
  fnormLS=apply(xLS,1,fnorm)
  
  # pick the first point which reduces fnorm compared to current point
  xnew=xLS[fnormLS <= fnorm(x),][1,]
  
  if (fnorm(xnew) <= 1e-10) { break }
  
  xiterrs=rbind(xiterrs,xnew)

}

# overlay newton's method trajectory on top of fnorm contour plot
filled.contour(x1,x2,Z,nlevels=25,xlab="x1",ylab="x2",
      plot.axes={axis(1); axis(2);
                 contour(x1,x2,Z,nlevels=25,add=T);
                 points(xiterrs[,1],xiterrs[,2],col="blue",pch=19);
                 lines(xiterrs[,1],xiterrs[,2],col="blue",lwd=2)})
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-4.png) 


## Session Information

```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] nleqslv_2.0 knitr_1.2  
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    stringr_0.6.2 
## [5] tools_3.0.1
```

