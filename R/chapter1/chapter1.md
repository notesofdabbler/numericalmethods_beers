Chapter 1: Linear Algebra
========================================================

Solve the system $Ax=b$ where:

$$ A=\left (\begin{array}{ccc}
    1 & 1 & 1 \\
    2 & 1 & 3 \\
    3 & 1 & 6 \\
   \end{array}
   \right)\;\;\;
   b=\left(\begin{array}{c}
              4 \\
              7 \\ 
              2
            \end{array}  
              \right)
$$


```r
# define A
A=matrix(c(1,1,1,
           2,1,3,
           3,1,6),ncol=3,byrow=TRUE)
A
```

```
##      [,1] [,2] [,3]
## [1,]    1    1    1
## [2,]    2    1    3
## [3,]    3    1    6
```

```r

#define b
b=c(4,7,2)
b
```

```
## [1] 4 7 2
```

```r

# Solve for x in Ax=b
x=solve(A,b)
x
```

```
## [1] 19 -7 -8
```


Perform LU decomposition of matrix $A=PLU$

```r
library(Matrix)
```

```
## Loading required package: lattice
```

```r
x = expand(lu(A))
L = as.matrix(x$L)
L
```

```
##        [,1] [,2] [,3]
## [1,] 1.0000  0.0    0
## [2,] 0.3333  1.0    0
## [3,] 0.6667  0.5    1
```

```r
U = as.matrix(x$U)
U
```

```
##      [,1]   [,2] [,3]
## [1,]    3 1.0000  6.0
## [2,]    0 0.6667 -1.0
## [3,]    0 0.0000 -0.5
```

```r
P = as.matrix(x$P)
P
```

```
##       [,1]  [,2]  [,3]
## [1,] FALSE  TRUE FALSE
## [2,] FALSE FALSE  TRUE
## [3,]  TRUE FALSE FALSE
```


Check that $A=PLU$

```r
test = P %*% L %*% U
test
```

```
##      [,1] [,2] [,3]
## [1,]    1    1    1
## [2,]    2    1    3
## [3,]    3    1    6
```


Now we solve for $x$ using the LU decomposition of $A$

$Ax=b \Rightarrow PLUx=b \Rightarrow x=U^{-1}(L^{-1}(P^{-1}b))$


```r
x = solve(U, solve(L, solve(P, b)))
x
```

```
## [1] 19 -7 -8
```


Cholesky decomposition of a positive-definite matrix $A=R^TR$


```r
# Define A
A=matrix(c(2,1,1,
           1,4,2,
           1,2,6),ncol=3,byrow=TRUE)
A
```

```
##      [,1] [,2] [,3]
## [1,]    2    1    1
## [2,]    1    4    2
## [3,]    1    2    6
```

```r
# cholesky factorization
R=chol(A)
R
```

```
##       [,1]   [,2]   [,3]
## [1,] 1.414 0.7071 0.7071
## [2,] 0.000 1.8708 0.8018
## [3,] 0.000 0.0000 2.2039
```

Solve the system $Ax=b$ using cholesky decomposition


```r
# define b
b = c(7, 15, 23)
x = solve(R, solve(t(R), b))
x
```

```
## [1] 1 2 3
```


## Modeling a separation system

The system of equations being solved is:
$$
\begin{eqnarray}
x_1+x_2+x_3 = 10 \\ 
0.04x_1+0.54x_2+0.26x_3=2 \\
0.93x_1+0.24x_2+0.0x_3=6
\end{eqnarray}
$$


```r
# Define A matrix
A=matrix(c(1,1,1,
           0.04,0.54,0.26,
           0.93,0.24,0.0),ncol=3,byrow=TRUE)
A
```

```
##      [,1] [,2] [,3]
## [1,] 1.00 1.00 1.00
## [2,] 0.04 0.54 0.26
## [3,] 0.93 0.24 0.00
```

```r
# define b vector
b=c(10,2,6)
# solve for x in Ax=b
x=solve(A,b)
x
```

```
## [1] 5.824 2.433 1.743
```


## Working with sparse matrices

Create a sparse banded diagonal matrix $A$ where the only non-zero elements are the following:
$$ 
   \begin{aligned}
   A_{i,i} &=2,& i=1,2,\ldots,N \\
   A_{i,i-1}&=-1,& i=2,\ldots,N \\
   A_{i,i+1}&=-1,& i=1,2,\ldots,N-1
   \end{aligned}
$$

```r
library(Matrix)

# Create a sparse matrix of dimension NxN that is banded diagonal
N=25
diags=list(rep(2,N),rep(-1,N-1),rep(-1,N-1))
A=bandSparse(N,k=c(0,-1,1),diag=diags,symm=FALSE)
image(A)
```

![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-8.png) 

Solving the system $Ax=b$

```r
# define b
b=rep(1,N)
x=solve(A,b)
plot(x)
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9.png) 


## Fill-in due to Gaussian Eliminations 
 
This example is covered in more detail in Chapter 6 and results from solving a partial differential equation using finite-differences on a 2-D grid consisting of $N_x$ points in the $x$ direction and $N_y$ points in $y$ direction. Thus there are $N_x\times N_y$ grid points in the 2-D space. The finite difference approach results in the following $A$ matrix.

$A$ is a $N\times N$ matrix with $N=N_x\times N_y$ with following non-zero entries
$$ 
 A_{n,n}=\left[\frac{2}{\Delta x^2}+\frac{2}{\Delta y^2}\right] \\
 A_{n,n-N_y}=A_{n,n+N_y}=\frac{-1}{\Delta x^2}  \\
 A_{n,n-1}=A_{n,n+1}=\frac{-1}{\Delta y^2}
$$

where $n=(i-1)\times N_y+j,\;i=2,\ldots,N_x-1,\;j=2,\ldots,N_y-1$

For points on the boundary of the 2-D domain $A_{n,n}=1$




```r
# set number of grid points Nx and Ny
Nx=15
Ny=15
N=Nx*Ny

# set dx and dy
dx=1/(Nx-1)
dy=1/(Ny-1)

# set up a sparse banded matrix
diag0=((2/dx^2)+(2/dy^2))
diag1=(-1/dy^2)
diag2=(-1/dx^2)

A=Matrix(0,nrow=N,ncol=N,sparse=TRUE)

# set A at boundary points
n=seq(0,Nx-1)*Ny+1
diag(A[n,n])=1
n=seq(0,Nx-1)*Ny+Ny
diag(A[n,n])=1
n=seq(1,Ny)
diag(A[n,n])=1
n=(Nx-1)*Ny+seq(1,Ny)
diag(A[n,n])=1

# set A at interior points
for (i in 2:(Nx-1)){
  for (j in 2:(Ny-1)){
    n=(i-1)*Ny+j
    A[n,n]=diag0
    A[n,n-1]=diag1
    A[n,n+1]=diag1
    A[n,n-Ny]=diag2
    A[n,n+Ny]=diag2
  }
  
}

image(A)
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10.png) 


LU decomposition of the matrix A illustrates fillin.The LU decomposition of sparse matrix using Matrix package in R seems to be different compared to MATLAB. The LU decomposition of a sparse matrix $A$ in R produces
$$ A=P^TLUQ $$ where $P,Q$ are permutation matrices


```r
lu=expand(lu(A))
image(lu$L,main="Matrix L")
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-111.png) 

```r
image(lu$U,main="Matrix U")
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-112.png) 

```r
image(lu$P,main="Matrix P")
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-113.png) 

```r
image(lu$Q,main="Matrix Q")
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-114.png) 

 
## Example: Solving a boundary value problem from fluid mechanics

The system here is represented by the following equations and boundary conditions
$$
\mu\frac{d^2v_x}{dx^2}-\left(\frac{\Delta P}{\Delta x}\right)=0 \\
v_x(y=0)=0,\;\;v_x(y=B)=V_{up}
$$
Using finite differences and assembling this system in the form $Ax=b$ results in the following system
$$
\left[
   \begin{array}{cccccc}
   2 & -1 & & & & \\
  -1 & 2 & -1 & & & \\
     & -1 & 2 & -1 & & \\
     & & \ddots & \ddots & \ddots & \\
     & & & -1 & 2 & -1 \\
     & & & & -1 & 2
    \end{array}
\right]
\left[
  \begin{array}{c}
  v_1 \\
  v_2 \\
  v_3 \\
  \vdots \\
  v_{N-1} \\
  v_N
  \end{array}
\right]
=
\left[
   \begin{array}{c}
   G+v_0 \\
   G \\
   G \\
   \vdots \\
   G \\
   G+v_{N+1}
   \end{array}
\right]
$$
where 
$$ 
   G=-\frac{\Delta y^2}{\mu}\left(\frac{\Delta P}{\Delta x}\right),\; 
   v_0=v_x(y=0)=0,\;
   v_{N+1}=v_x(y=B)=V_{up}
$$


```r
# based on simple_1D_flow.m from book resource site

# set system parameters
visc=1.0e-3 # viscosity in Pa.s
rho=1000 # density in kg/m3
V_up=0 # velocity of upper plate m/s
B=1/1e3 # Distance between plates in m
dp_dx=-1 

# set simulation parameters
N=25
dy=B/(N+1)

# set A matrix (in sparse format)
A=bandSparse(N,k=c(0,1),
              diag=list(rep(2,N),
                     rep(-1,N)),
              symm=TRUE 
             )

# set b matrix
G=-(dy^2)/visc*dp_dx
b=rep(G,N)
b[N]=b[N]+V_up

# Solve the system
v=solve(A,b)

# calculate reynolds number
Renum=rho*mean(v)*(2*B)/visc

# plot velocity
y=seq(0,B,length.out=N+2)
vx=c(0,as.numeric(v),V_up)
plot(y,vx,type="l",xaxt="n",yaxt="n",
     xlab=expression(paste("y (m) [x",10^{-3},"]")),
     ylab=expression(paste("vx(y) (m/s) [",10^{-4},"]")))
axis(1,at=seq(0,1,0.1)*1e-3,label=seq(0,1,0.1))
axis(2,at=seq(0,1.2,0.2)*1e-4,label=seq(0,1.2,0.2))
text(0.4e-3,0.6e-4,paste("Re=",round(Renum,4),sep=""))
text(0.4e-3,0.4e-4,paste("dp/dx=",dp_dx,sep=""))
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12.png) 



```r
sessionInfo()
```

```
## R version 3.0.1 (2013-05-16)
## Platform: i386-w64-mingw32/i386 (32-bit)
## 
## locale:
## [1] LC_COLLATE=English_United States.1252 
## [2] LC_CTYPE=English_United States.1252   
## [3] LC_MONETARY=English_United States.1252
## [4] LC_NUMERIC=C                          
## [5] LC_TIME=English_United States.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] Matrix_1.0-12   lattice_0.20-15 knitr_1.2      
## 
## loaded via a namespace (and not attached):
## [1] digest_0.6.3   evaluate_0.4.3 formatR_0.8    grid_3.0.1    
## [5] stringr_0.6.2  tools_3.0.1
```

