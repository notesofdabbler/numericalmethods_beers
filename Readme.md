In this repository, I hope to learn R and Python by recasting the solved examples in MATLAB from the following book into R and Python.

Numerical Methods for Chemical Engineering - Applications in MATLAB by Kenneth J. Beers, Cambridge University Press, 2007